# Policy management demo app

## Instructions
Can be found here: https://gitlab.com/kimjc/rps-code-demo/wikis/Running-the-demo


## Original Requirements
[Are located here.](https://gitlab.com/kimjc/rps-code-demo/blob/master/requirements.md)  