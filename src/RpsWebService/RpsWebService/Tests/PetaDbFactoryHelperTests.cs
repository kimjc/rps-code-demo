﻿using System.Diagnostics.CodeAnalysis;
using Moq;
using NUnit.Framework;
using RpsWebService.Interfaces;
using RpsWebService.Models;
using RpsWebService.Services;
using System.Collections.Generic;
using System.Linq;

namespace RpsWebService.Tests
{
    [ExcludeFromCodeCoverage]
    [TestFixture]
    public class PetaDbFactoryHelperTests
    {
        [Test]
        public void TestGetAddress()
        {
            var db = new Mock<IPetaDatabase>();
            var verifyAddress = new Address
            {
                AddressId = 1,
                MailingAddress = "test mailing address",
                City = "test city",
                State = "test state",
                Postal = "test postal"
            };

            db.Setup(d => d.FirstOrDefault<Address>(It.IsAny<string>(), It.IsAny<object[]>()))
                .Returns(verifyAddress)
                .Verifiable();

            var helper = new PetaDbBridgeHelper(db.Object);
            var address = helper.GetAddress(1);

            // assert that mock return value matches expected
            Assert.That(address, Is.EqualTo(verifyAddress));

            // validate the wirings are still intact
            db.Verify(d => d.FirstOrDefault<Address>(It.IsAny<string>(), It.IsAny<object[]>()));
        }

        [Test]
        public void TestGetInsured()
        {
            var db = new Mock<IPetaDatabase>();

            var verifyInsured = new Insured
            {
                InsuredId = 1,
                FirstName = "Homer",
                LastName = "Simpson"
            };

            db.Setup(d => d.FirstOrDefault<Insured>(It.IsAny<string>(), It.IsAny<object[]>()))
                .Returns(verifyInsured)
                .Verifiable();

            var helper = new PetaDbBridgeHelper(db.Object);
            var insured = helper.GetInsured(1);

            // assert that mock return value matches expected
            Assert.That(insured, Is.EqualTo(verifyInsured));

            // validate the wirings are still intact
            db.Verify(d => d.FirstOrDefault<Insured>(It.IsAny<string>(), It.IsAny<object[]>()));
        }

        [Test]
        public void TestGetPoliciesByQuery()
        {
            var db = new Mock<IPetaDatabase>();

            // barebones policy object; we're just verifying this reference survives the wireups/logic branching
            var verifyPolicy = new InsurancePolicy
            {
                PolicyId = 1,
                PolicyNumber = "12345"
            };

            db.Setup(d => d.Fetch<InsurancePolicy>(It.IsAny<string>(), It.IsAny<object[]>()))
                .Returns(new List<InsurancePolicy>{verifyPolicy}.ToList())
                .Verifiable();

            var helper = new PetaDbBridgeHelper(db.Object);
            var policyList = helper.GetPoliciesByQuery("select * from poilcy where policy_id = @0", 1);

            // assert that mock return value matches expected
            Assert.That(policyList.FirstOrDefault(), Is.EqualTo(verifyPolicy));

            // validate the wirings are still intact
            db.Verify(d => d.Fetch<InsurancePolicy>(It.IsAny<string>(), It.IsAny<object[]>()));
        }

        [Test]
        public void TestHydratePolicies()
        {
            var db = new Mock<IPetaDatabase>();
            var verifyInsured = new Insured
            {
                InsuredId = 1,
                FirstName = "Homer",
                LastName = "Simpson"
            };

            db.Setup(d => d.FirstOrDefault<Insured>(It.IsAny<string>(), It.IsAny<object[]>()))
                .Returns(verifyInsured)
                .Verifiable();

            var verifyAddress = new Address
            {
                AddressId = 1,
                MailingAddress = "test mailing address",
                City = "test city",
                State = "test state",
                Postal = "test postal"
            };

            db.Setup(d => d.FirstOrDefault<Address>(It.IsAny<string>(), It.IsAny<object[]>()))
                .Returns(verifyAddress)
                .Verifiable();

            var helper = new PetaDbBridgeHelper(db.Object);

            // the policy to test hydration against
            var policy = new InsurancePolicy
            {
                PolicyId = 1,
                PolicyNumber = "12345",
                RiskAddressId = 1,
                InsuredId = 1
            };

            var policyList = new List<InsurancePolicy> {policy};

            helper.HydratePolicies(policyList);

            // assert that mock return value matches expected
            Assert.That(policy.Insured, Is.EqualTo(verifyInsured));
            Assert.That(policy.Insured.MailingAddress, Is.EqualTo(verifyAddress));
            Assert.That(policy.RiskAddress, Is.EqualTo(verifyAddress));

            // validate the wirings are still intact
            db.Verify(d => d.FirstOrDefault<Address>(It.IsAny<string>(), It.IsAny<object[]>()));
            db.Verify(d => d.FirstOrDefault<Insured>(It.IsAny<string>(), It.IsAny<object[]>()));
        }
    }
}