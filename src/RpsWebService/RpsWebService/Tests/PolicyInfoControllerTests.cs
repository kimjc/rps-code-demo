﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using RpsWebService.Controllers;
using RpsWebService.Interfaces;
using RpsWebService.Models;
using RpsWebService.Services;

namespace RpsWebService.Tests
{
    [ExcludeFromCodeCoverage]
    [TestFixture]
    public class PolicyInfoControllerTests
    {
        [Test]
        public void TestDeletePolicy()
        {
            var repository = new Mock<IPolicyRepository>();

            repository.Setup(r => r.DeletePolicy(It.IsAny<string>()))
                .Verifiable();

            var controller = new PolicyInfoController(repository.Object);
            var result = controller.Delete("12345");

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(result, Is.TypeOf<OkResult>());

            // validate the wirings are still intact
            repository.Verify(r => r.DeletePolicy(It.IsAny<string>()));
        }

        [Test]
        public void TestDeletePolicyFail()
        {
            var repository = new Mock<IPolicyRepository>();

            repository.Setup(r => r.DeletePolicy(It.IsAny<string>()))
                .Throws<Exception>()
                .Verifiable();

            var controller = new PolicyInfoController(repository.Object);
            var result = controller.Delete("12345");

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(result, Is.TypeOf<BadRequestObjectResult>());

            // validate the wirings are still intact
            repository.Verify(r => r.DeletePolicy(It.IsAny<string>()));
        }

        [Test]
        public void TestGetAllPolicies()
        {
            var repository = new Mock<IPolicyRepository>();
            var verifyPolicy = new InsurancePolicy { PolicyId = 1, PolicyNumber = "123" };

            repository.Setup(r => r.GetAllPolicies())
                .Returns(new List<InsurancePolicy> { verifyPolicy }.ToList())
                .Verifiable();

            var controller = new PolicyInfoController(repository.Object);
            var policies = controller.Get();

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(policies.FirstOrDefault(), Is.EqualTo(verifyPolicy));

            // validate the wirings are still intact
            repository.Verify(r => r.GetAllPolicies());
        }

        [Test]
        public void TestGetPolicyByName()
        {
            var repository = new Mock<IPolicyRepository>();

            var verifyPolicy = new InsurancePolicy { PolicyId = 1, PolicyNumber = "123" };

            repository.Setup(f => f.GetPoliciesByName(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<InsurancePolicy> { verifyPolicy }.ToList())
                .Verifiable();

            var controller = new PolicyInfoController(repository.Object);
            var policies = controller.Get("simpson", "homer");

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(policies.FirstOrDefault(), Is.EqualTo(verifyPolicy));

            // validate the wirings are still intact
            repository.Verify(f => f.GetPoliciesByName(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Test]
        public void TestGetPolicyByNumber()
        {
            var repository = new Mock<IPolicyRepository>();

            var verifyPolicy = new InsurancePolicy { PolicyId = 1, PolicyNumber = "12345" };

            repository.Setup(f => f.GetPoliciesByNumber(It.IsAny<string>()))
                .Returns(new List<InsurancePolicy> { verifyPolicy }.ToList())
                .Verifiable();

            var controller = new PolicyInfoController(repository.Object);
            var policies = controller.GetByPolicyNumber("12345");

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(policies.FirstOrDefault(), Is.EqualTo(verifyPolicy));

            // validate the wirings are still intact
            repository.Verify(f => f.GetPoliciesByNumber(It.IsAny<string>()));
        }

        [Test]
        public void TestSavePolicy()
        {
            var repository = new Mock<IPolicyRepository>();

            var verifyPolicy = new InsurancePolicy { PolicyId = 1, PolicyNumber = "12345" };

            repository.Setup(f => f.SavePolicy(It.IsAny<InsurancePolicy>()))
                .Verifiable();

            var controller = new PolicyInfoController(repository.Object);
            var result = controller.Put(verifyPolicy);

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(result, Is.TypeOf<OkResult>());

            // validate the wirings are still intact
            repository.Verify(f => f.SavePolicy(It.IsAny<InsurancePolicy>()));
        }

        [Test]
        public void TestSavePolicyFail()
        {
            var repository = new Mock<IPolicyRepository>();

            var verifyPolicy = new InsurancePolicy { PolicyId = 1, PolicyNumber = "12345" };

            repository.Setup(f => f.SavePolicy(It.IsAny<InsurancePolicy>()))
                .Throws<Exception>()
                .Verifiable();

            var controller = new PolicyInfoController(repository.Object);
            var result = controller.Put(verifyPolicy);

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(result, Is.TypeOf<BadRequestObjectResult>());

            // validate the wirings are still intact
            repository.Verify(f => f.SavePolicy(It.IsAny<InsurancePolicy>()));
        }

        [Test]
        public void TestSavePolicyInBulk()
        {
            var repository = new Mock<IPolicyRepository>();

            var verifyPolicy = new InsurancePolicy { PolicyId = 1, PolicyNumber = "12345" };
            var verifyPolicy2 = new InsurancePolicy { PolicyId = 1, PolicyNumber = "12345" };

            repository.Setup(f => f.SavePolicy(It.IsAny<InsurancePolicy>()))
                .Verifiable();

            var controller = new PolicyInfoController(repository.Object);
            var result = controller.Put(new List<InsurancePolicy>{verifyPolicy, verifyPolicy2});

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(result, Is.TypeOf<OkResult>());

            // validate the wirings are still intact
            repository.Verify(f => f.SavePolicy(It.IsAny<InsurancePolicy>()));
        }

        [Test]
        public void TestSavePolicyInBulkFail()
        {
            var repository = new Mock<IPolicyRepository>();

            var verifyPolicy = new InsurancePolicy { PolicyId = 1, PolicyNumber = "12345" };
            var verifyPolicy2 = new InsurancePolicy { PolicyId = 1, PolicyNumber = "12345" };

            repository.Setup(f => f.SavePolicy(It.IsAny<InsurancePolicy>()))
                .Throws<Exception>()
                .Verifiable();

            var controller = new PolicyInfoController(repository.Object);
            var result = controller.Put(new List<InsurancePolicy> { verifyPolicy, verifyPolicy2 });

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(result, Is.TypeOf<BadRequestObjectResult>());

            // validate the wirings are still intact
            repository.Verify(f => f.SavePolicy(It.IsAny<InsurancePolicy>()));
        }
    }
}