﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Moq;
using NUnit.Framework;
using RpsWebService.Interfaces;
using RpsWebService.Models;
using RpsWebService.Services;

namespace RpsWebService.Tests
{
    [ExcludeFromCodeCoverage]
    [TestFixture]
    public class PolicyRepositoryTests
    {
        [Test]
        public void TestGetAllPolicies()
        {
            var factory = new Mock<IPetaDbBridge>();

            var verifyPolicy = new InsurancePolicy {PolicyId = 1, PolicyNumber = "123"};
            
            factory.Setup(f => f.GetAllPolicies())
                .Returns(new List<InsurancePolicy>{verifyPolicy}.ToList())
                .Verifiable();

            var repository = new PolicyRepository(factory.Object);
            var policies = repository.GetAllPolicies();

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(policies.FirstOrDefault(), Is.EqualTo(verifyPolicy));

            // validate the wirings are still intact
            factory.Verify(f => f.GetAllPolicies());
        }

        [Test]
        public void TestGetPoliciesByName()
        {
            var factory = new Mock<IPetaDbBridge>();

            var verifyPolicy = new InsurancePolicy { PolicyId = 1, PolicyNumber = "123" };

            factory.Setup(f => f.GetPoliciesByName(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<InsurancePolicy> { verifyPolicy }.ToList())
                .Verifiable();

            var repository = new PolicyRepository(factory.Object);
            var policies = repository.GetPoliciesByName("simpson", "homer");

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(policies.FirstOrDefault(), Is.EqualTo(verifyPolicy));

            // validate the wirings are still intact
            factory.Verify(f => f.GetPoliciesByName(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Test]
        public void TestGetPoliciesByNumber()
        {
            var factory = new Mock<IPetaDbBridge>();

            var verifyPolicy = new InsurancePolicy { PolicyId = 1, PolicyNumber = "123" };

            factory.Setup(f => f.GetPoliciesByNumber(It.IsAny<string>()))
                .Returns(new List<InsurancePolicy> { verifyPolicy }.ToList())
                .Verifiable();

            var repository = new PolicyRepository(factory.Object);
            var policies = repository.GetPoliciesByNumber("12345");

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(policies.FirstOrDefault(), Is.EqualTo(verifyPolicy));

            // validate the wirings are still intact
            factory.Verify(f => f.GetPoliciesByNumber(It.IsAny<string>()));
        }

        [Test]
        public void TestSaveExistingPolicy()
        {
            var factory = new Mock<IPetaDbBridge>();
            var verifyPolicy = new InsurancePolicy { PolicyId = 1, PolicyNumber = "123" };

            factory.Setup(f => f.Save(It.IsAny<InsurancePolicy>()))
                .Returns(1)
                .Verifiable();

            factory.Setup(f => f.IsNew(It.IsAny<InsurancePolicy>()))
                .Returns(false)
                .Verifiable();

            var repository = new PolicyRepository(factory.Object);
            repository.SavePolicy(verifyPolicy);

            Assert.That(verifyPolicy.PolicyId, Is.EqualTo(1));

            // validate the wirings are still intact
            factory.Verify(f => f.Save(It.IsAny<InsurancePolicy>()));
        }

        [Test]
        public void TestSaveNewPolicy()
        {
            var factory = new Mock<IPetaDbBridge>();

            var verifyAddress = new Address
            {
                AddressId = 1
            };

            var verifyPolicy = new InsurancePolicy
            {
                PolicyId = 1,
                PolicyNumber = "123",
                Insured = new Insured
                {
                    MailingAddress = verifyAddress
                },
                RiskAddress = verifyAddress
            };

            factory.Setup(f => f.Save(It.IsAny<InsurancePolicy>()))
                .Returns(1)
                .Verifiable();

            factory.Setup(f => f.IsNew(It.IsAny<InsurancePolicy>()))
                .Returns(true)
                .Verifiable();

            var repository = new PolicyRepository(factory.Object);
            repository.SavePolicy(verifyPolicy);

            Assert.That(verifyPolicy.PolicyId, Is.EqualTo(1));

            // validate the wirings are still intact
            factory.Verify(f => f.Save(It.IsAny<InsurancePolicy>()));
        }


        [Test]
        public void TestDeleteExistingPolicy()
        {
            var factory = new Mock<IPetaDbBridge>();
            var verifyPolicy = new InsurancePolicy { PolicyId = 1, PolicyNumber = "12345" };
            factory.Setup(f => f.Delete(It.IsAny<InsurancePolicy>()))
                .Verifiable();

            factory.Setup(f => f.GetPoliciesByNumber(It.IsAny<string>()))
                .Returns(new List<InsurancePolicy> { verifyPolicy }.ToList())
                .Verifiable();

            var repository = new PolicyRepository(factory.Object);
            repository.DeletePolicy("12345");

            // validate the wirings are still intact
            factory.Verify(f => f.Delete(It.IsAny<InsurancePolicy>()));
        }

        [Test]
        public void TestDeleteNonExistingPolicy()
        {
            var factory = new Mock<IPetaDbBridge>();
            var verifyPolicy = new InsurancePolicy { PolicyId = 1, PolicyNumber = "123" };
            factory.Setup(f => f.Delete(It.IsAny<InsurancePolicy>()))
                .Verifiable();

            factory.Setup(f => f.GetPoliciesByNumber(It.IsAny<string>()))
                .Returns(new List<InsurancePolicy> { verifyPolicy }.ToList())
                .Verifiable();

            var repository = new PolicyRepository(factory.Object);

            // attempting to delete non-existing policy should throw exception
            Assert.Throws<Exception>(() => repository.DeletePolicy("12345"));
        }
    }
}