﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using RpsWebService.Interfaces;
using RpsWebService.Models;
using RpsWebService.Services;

namespace RpsWebService.Tests
{
    [ExcludeFromCodeCoverage]
    [TestFixture]
    public class PetaDbFactoryTests
    {
        [Test]
        public void TestGetAllPolicies()
        {
            var helper = new Mock<IPetaDbBridgeHelper>();
            var db = new Mock<IPetaDatabase>();

            helper.Setup(h => h.GetPoliciesByQuery(It.IsAny<string>(), It.IsAny<object[]>()))
                .Returns(new List<InsurancePolicy>
                {
                    new InsurancePolicy {PolicyId = 1, PolicyNumber = "123"},
                    new InsurancePolicy {PolicyId = 2, PolicyNumber = "456"}
                })
                .Verifiable();

            var factory = new PetaDbBridge(helper.Object, db.Object);
            var policies = factory.GetAllPolicies();

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(policies.Count, Is.EqualTo(2));

            // validate the wirings are still intact
            helper.Verify(h => h.GetPoliciesByQuery(It.IsAny<string>(), It.IsAny<object[]>()));
        }

        [Test]
        public void TestGetPoliciesByNumber()
        {
            var helper = new Mock<IPetaDbBridgeHelper>();
            var db = new Mock<IPetaDatabase>();

            helper.Setup(h => h.GetPoliciesByQuery(It.IsAny<string>(), new object[]{"123%"}))
                .Returns(new List<InsurancePolicy>
                {
                    new InsurancePolicy {PolicyId = 1, PolicyNumber = "123"},
                });

            helper.Setup(h => h.GetPoliciesByQuery(It.IsAny<string>(), new object[] { "456%" }))
                .Returns(new List<InsurancePolicy>
                {
                    new InsurancePolicy {PolicyId = 2, PolicyNumber = "456"}
                });

            var factory = new PetaDbBridge(helper.Object, db.Object);
            var policies = factory.GetPoliciesByNumber("123");

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(policies.Count, Is.EqualTo(1));
        }

        [Test]
        public void TestGetPoliciesByName()
        {
            var helper = new Mock<IPetaDbBridgeHelper>();
            var db = new Mock<IPetaDatabase>();

            helper.Setup(h => h.GetPoliciesByQuery(It.IsAny<string>(), new object[] { "simpson%", "homer%" }))
                .Returns(new List<InsurancePolicy>
                {
                    new InsurancePolicy {PolicyId = 1, PolicyNumber = "123"},
                });

            helper.Setup(h => h.GetPoliciesByQuery(It.IsAny<string>(), new object[] { "flanders%", "ned%" }))
                .Returns(new List<InsurancePolicy>
                {
                    new InsurancePolicy {PolicyId = 2, PolicyNumber = "456"}
                });


            var factory = new PetaDbBridge(helper.Object, db.Object);
            var policies = factory.GetPoliciesByName("simpson", "homer");

            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(policies.Count, Is.EqualTo(1));
        }

        [Test]
        public void TestGetInsured()
        {
            var helper = new Mock<IPetaDbBridgeHelper>();
            var db = new Mock<IPetaDatabase>();

            var verifyInsured = new Insured
            {
                InsuredId = 1,
                FirstName = "Homer",
                LastName = "Simpson"
            };

            helper.Setup(h => h.GetInsured(It.IsAny<int>()))
                .Returns(verifyInsured)
                .Verifiable();

            var factory = new PetaDbBridge(helper.Object, db.Object);
            var insured = factory.GetInsured(1);
            
            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(insured, Is.Not.Null);

            // validate the wirings are still intact
            helper.Verify(h => h.GetInsured(It.IsAny<int>()));
        }

        [Test]
        public void TestGetAddress()
        {
            var helper = new Mock<IPetaDbBridgeHelper>();
            var db = new Mock<IPetaDatabase>();

            var verifyAddress = new Address
            {
                AddressId = 1,
                MailingAddress = "742 Evergreen Terrace",
                City = "Springfield",
                State = "Oregon",
                Postal = "12345"
            };

            helper.Setup(h => h.GetAddress(It.IsAny<int>()))
                .Returns(verifyAddress)
                .Verifiable();

            var factory = new PetaDbBridge(helper.Object, db.Object);
            var address = factory.GetAddress(1);
            
            // validate expected result (aka nothing has mutated unexpectedly...)
            Assert.That(address, Is.EqualTo(verifyAddress));

            // validate the wirings are still intact
            helper.Verify(h => h.GetAddress(It.IsAny<int>()));
        }

        [Test]
        public void TestSave()
        {
            var helper = new Mock<IPetaDbBridgeHelper>();
            var db = new Mock<IPetaDatabase>();

            db.Setup(d => d.Save(It.IsAny<InsurancePolicy>()))
                .Verifiable();

            var factory = new PetaDbBridge(helper.Object, db.Object);
            factory.Save(new InsurancePolicy());

            // validate the wirings are still intact
            db.Verify(d => d.Save(It.IsAny<InsurancePolicy>()));
        }

        [Test]
        public void TestIsNew()
        {
            var helper = new Mock<IPetaDbBridgeHelper>();
            var db = new Mock<IPetaDatabase>();

            db.Setup(d => d.IsNew(It.IsAny<InsurancePolicy>()))
                .Returns(true)
                .Verifiable();

            var factory = new PetaDbBridge(helper.Object, db.Object);
            var isNew = factory.IsNew(new InsurancePolicy());

            // should return true
            Assert.That(isNew, Is.True);

            // validate the wirings are still intact
            db.Verify(d => d.IsNew(It.IsAny<InsurancePolicy>()));
        }

        [Test]
        public void TestDelete()
        {
            var helper = new Mock<IPetaDbBridgeHelper>();
            var db = new Mock<IPetaDatabase>();

            db.Setup(d => d.Delete(It.IsAny<InsurancePolicy>()))
                .Verifiable();

            var factory = new PetaDbBridge(helper.Object, db.Object);
            factory.Delete(new InsurancePolicy());

            // validate the wirings are still intact
            db.Verify(d => d.Delete(It.IsAny<InsurancePolicy>()));
        }
    }
}
