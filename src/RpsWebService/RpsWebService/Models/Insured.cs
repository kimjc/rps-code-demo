﻿using System.Diagnostics.CodeAnalysis;
using PetaPoco.NetCore;

namespace RpsWebService.Models
{
    [ExcludeFromCodeCoverage]
    [TableName("Insured")]
    [PrimaryKey("insured_id", autoIncrement = true)]
    public class Insured
    {
        [Column(Name = "insured_id")]
        public int InsuredId { get; set; }
        [Column(Name = "first_name")]
        public string FirstName { get; set; }
        [Column(Name = "last_name")]
        public string LastName { get; set; }
        [Column(Name = "address_id")]
        public int AddressId { get; set; }

        [Ignore]
        public Address MailingAddress { get; set; }
    }
}
