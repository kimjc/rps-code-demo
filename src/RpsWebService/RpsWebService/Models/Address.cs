﻿using System.Diagnostics.CodeAnalysis;
using PetaPoco.NetCore;

namespace RpsWebService.Models
{
    [ExcludeFromCodeCoverage]
    [TableName("address")]
    [PrimaryKey("address_id", autoIncrement = true)]
    public class Address
    {
        [Column(Name = "address_id")]
        public int AddressId { get; set; }
        [Column(Name = "mailing_address")]
        public string MailingAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postal { get; set; }
    }
}
