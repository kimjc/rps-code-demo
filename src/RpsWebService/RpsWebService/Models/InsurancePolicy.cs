﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using PetaPoco.NetCore;

namespace RpsWebService.Models
{
    [ExcludeFromCodeCoverage]
    [TableName("policy")]
    [PrimaryKey("policy_id", autoIncrement = true)]
    public class InsurancePolicy
    {
        private RiskConstruction riskConstructionEnum;

        [Column("policy_id")]
        public int PolicyId { get; set; }
        [Column("policy_number")]
        public string PolicyNumber { get; set; }
        [Column(Name = "effective_date")]
        public DateTime EffectiveDate { get; set; }
        [Column(Name = "expiration_date")]
        public DateTime ExpirationDate { get; set; }

        [Column(Name = "insured_id")]
        public int InsuredId { get; set; }

        [Ignore]
        public Insured Insured { get; set; }

        [Column(Name = "risk_construction")]
        public string RiskConstruction
        {
            get => riskConstructionEnum.ToString();
            set => Enum.TryParse(value, out riskConstructionEnum);
        }

        [Column(Name = "risk_year_built")]
        public int RiskYearBuilt { get; set; }

        [Column(Name = "risk_address_id")]
        public int RiskAddressId { get; set; }

        [Ignore]
        public Address RiskAddress { get; set; }

    }
}
