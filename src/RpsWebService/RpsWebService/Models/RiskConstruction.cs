﻿using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RpsWebService.Models
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RiskConstruction
    {
        [Description("Site Built Home")]
        SiteBuiltHome,
        [Description("Modular Home")]
        ModularHome,
        [Description("Single Wide Manufactured Home")]
        SingleWideManufacturedHome,
        [Description("Double Wide Manufactured Home")]
        DoubleWideManufacturedHome
    }
}