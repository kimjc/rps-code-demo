﻿using System.Collections.Generic;
using RpsWebService.Models;

namespace RpsWebService.Interfaces
{
    public interface IPolicyRepository
    {
        IList<InsurancePolicy> GetAllPolicies();
        IList<InsurancePolicy> GetPoliciesByNumber(string policyNumber);
        IList<InsurancePolicy> GetPoliciesByName(string lastName, string firstName);
        void SavePolicy(InsurancePolicy policy);
        void DeletePolicy(string policyNumber);
    }
}