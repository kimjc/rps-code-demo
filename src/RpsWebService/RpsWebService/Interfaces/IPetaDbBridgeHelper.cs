﻿using System.Collections.Generic;
using RpsWebService.Models;

namespace RpsWebService.Interfaces
{
    public interface IPetaDbBridgeHelper
    {
        Address GetAddress(int addressId);
        Insured GetInsured(int insuredId);
        IList<InsurancePolicy> GetPoliciesByQuery(string query, params object[] args);
        void HydratePolicies(IList<InsurancePolicy> policies);
    }
}