﻿using System.Collections.Generic;

namespace RpsWebService.Interfaces
{
    public interface IPetaDatabase
    {
        bool IsNew<T>(T payload);
        int Save<T>(T payload);
        void Delete<T>(T payload);
        T FirstOrDefault<T>(string query, params object[] args);
        IEnumerable<T> Fetch<T>(string query, params object[] args);
    }
}