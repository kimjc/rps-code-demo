using System.Collections.Generic;
using RpsWebService.Models;

namespace RpsWebService.Interfaces
{
    public interface IPetaDbBridge
    {
        Insured GetInsured(int insuredId);
        Address GetAddress(int addressId);
        IList<InsurancePolicy> GetAllPolicies();
        IList<InsurancePolicy> GetPoliciesByNumber(string policyNumber);
        IList<InsurancePolicy> GetPoliciesByName(string lastName, string firstName);
        int Save<T>(T payload);
        void Delete<T>(T payload);
        bool IsNew<T>(T payload);
    }
}