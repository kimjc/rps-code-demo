﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using RpsWebService.Interfaces;
using RpsWebService.Models;

namespace RpsWebService.Controllers
{
    // REST endpoint definitions for Policy Info
    // All return values in application/json
    [Produces("application/json")]
    [Route("api/v1/policy")]
    public class PolicyInfoController : Controller
    {
        private readonly IPolicyRepository repository;

        public PolicyInfoController(IPolicyRepository repository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        // GET api/v1/policy
        /// <summary>
        /// Get all policies in the data store.
        /// </summary>
        /// <returns>List of matching policies</returns>
        
        [HttpGet]
        public IList<InsurancePolicy> Get()
        {
            return repository.GetAllPolicies();
        }

        // GET api/v1/policy/12345
        /// <summary>
        /// Find policies that begin with the matching policy number.
        /// </summary>
        /// <param name="policyNumber" description="Policy number starting value to match against."></param>
        /// <returns>List of matching policies</returns>
        [HttpGet("{policyNumber}")]
        public IList<InsurancePolicy> GetByPolicyNumber(string policyNumber)
        {
            return repository.GetPoliciesByNumber(policyNumber);
        }

        // GET api/v1/policy/Doe/John
        /// <summary>
        /// Find policies that begin with the matching first and last name values.
        /// </summary>
        /// <param name="lastName" description="Last name starting value to match against."></param>
        /// <param name="firstName" descrription="First name starting value to match against."></param>
        /// <returns>List of matching policies</returns>
        [HttpGet("{lastName}/{firstName}")]
        public IList<InsurancePolicy> Get(string lastName, string firstName)
        {
            return repository.GetPoliciesByName(lastName, firstName);
        }

        // PUT api/v1/policy
        /// <summary>
        /// Add or update a policy.
        /// </summary>
        /// <param name="policy" description="InsurancePolicy JSON object to add."></param>
        /// <returns>Result status code</returns>
        [HttpPut] 
        public IActionResult Put([FromBody] InsurancePolicy policy)
        {
            try
            {
                repository.SavePolicy(policy);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add/update multiple policies to the data store.
        /// </summary>
        /// <param name="policies" description="Collection of InsurancePolicy JSON object to add/update."></param>
        /// <returns>Result status code</returns>
        [HttpPut("bulkput")]
        public IActionResult Put([FromBody] IList<InsurancePolicy> policies)
        {
            try
            {
                foreach (var policy in policies)
                {
                    repository.SavePolicy(policy);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/v1/policy/12345
        /// <summary>
        /// Delete a specific policy record by policy number.
        /// </summary>
        /// <param name="policyNumber"></param>
        /// <returns></returns>
        [HttpDelete]
        public IActionResult Delete(string policyNumber)
        {
            try
            {
                repository.DeletePolicy(policyNumber);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
