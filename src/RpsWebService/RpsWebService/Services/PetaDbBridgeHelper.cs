﻿using System;
using System.Collections.Generic;
using System.Linq;
using RpsWebService.Interfaces;
using RpsWebService.Models;

namespace RpsWebService.Services
{
    /// <summary>
    /// Helper class for the petadbbridge
    /// </summary>
    public class PetaDbBridgeHelper : IPetaDbBridgeHelper
    {
        private readonly IPetaDatabase db;

        public PetaDbBridgeHelper(IPetaDatabase db)
        {
            this.db = db ?? throw new ArgumentNullException(nameof(db));
        }

        /// <summary>
        /// Fetches policies and ensures that the entire object graph including insured and risk address get populated correctly
        /// </summary>
        /// <param name="query"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public IList<InsurancePolicy> GetPoliciesByQuery(string query, params object[] args)
        {
            var found = args == null || !args.Any()
                ? db.Fetch<InsurancePolicy>(query).ToList()
                : db.Fetch<InsurancePolicy>(query, args).ToList();

            HydratePolicies(found);
            
            return found;
        }

        /// <summary>
        /// Fetches insured information from the persistence layer
        /// </summary>
        /// <param name="insuredId"></param>
        /// <returns></returns>
        public Insured GetInsured(int insuredId)
        {
            var found = db.FirstOrDefault<Insured>("select * from insured where insured_id = @0", insuredId);
            return found;
        }

        /// <summary>
        /// Fetches address information from the persistence layer
        /// </summary>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public Address GetAddress(int addressId)
        {
            var found = db.FirstOrDefault<Address>("select * from address where address_id = @0", addressId);
            return found;
        }

        /// <summary>
        /// Fully hydrate the policy object - filling in the insured and risk address graphs
        /// </summary>
        /// <param name="policies"></param>
        public void HydratePolicies(IList<InsurancePolicy> policies)
        {
            foreach (var policy in policies)
            {
                var insured = GetInsured(policy.InsuredId);
                if (insured != null)
                {
                    var insuredAddress = GetAddress(insured.AddressId);
                    insured.MailingAddress = insuredAddress;
                }
                policy.Insured = insured;

                var riskAddress = GetAddress(policy.RiskAddressId);
                policy.RiskAddress = riskAddress;
            }
        }
    }
}