﻿using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using PetaPoco.NetCore;
using RpsWebService.Interfaces;

namespace RpsWebService.Services
{
    /// <summary>
    /// Facade layer covering the PetaPoco database implementation
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PetaDatabase : IPetaDatabase
    {
        private static string connectionString;
        private readonly Database db;

        /// <summary>
        /// Set the connection string
        /// </summary>
        /// <param name="dbString"></param>
        public static void SetConnectionString(string dbString)
        {
            connectionString = dbString;
        }
        
        public PetaDatabase()
        {
            db = new Database(connectionString, "System.Data.SqlClient");
        }

        /// <summary>
        /// Check if the POCO is new or already exists in the database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="payload"></param>
        /// <returns></returns>
        public bool IsNew<T>(T payload)
        {
            return db.IsNew(payload);
        }

        /// <summary>
        /// Save the POCO to the db
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="payload"></param>
        /// <returns></returns>
        public int Save<T>(T payload)
        {
            if (db.IsNew(payload))
            {
                return db.Save(payload);
            }
            else
            {
                return db.Update(payload);
            }
        }

        /// <summary>
        /// Delete the POCO from the db
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="payload"></param>
        public void Delete<T>(T payload)
        {
            db.Delete(payload);
        }

        /// <summary>
        /// Fetch the mapped POCO from the db using optional search parameters - refer to PetaPoco documentation on the .Query&lt;T&gt;() for details.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public IEnumerable<T> Fetch<T>(string query, params object[] args)
        {
            return db.Fetch<T>(query, args);
        }

        /// <summary>
        /// Fetch the first matching mapped POCO from the db.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public T FirstOrDefault<T>(string query, params object[] args)
        {
            return db.FirstOrDefault<T>(query, args);
        }
    }
}