﻿using System;
using System.Collections.Generic;
using System.Linq;
using RpsWebService.Interfaces;
using RpsWebService.Models;

namespace RpsWebService.Services
{
    /// <summary>
    /// The mediating business logic layer between the REST controller and the backing persistence layer
    /// </summary>
    public class PolicyRepository : IPolicyRepository
    {
        private readonly IPetaDbBridge petaDbBridge;

        public PolicyRepository(IPetaDbBridge petaDbBridge)
        {
            this.petaDbBridge = petaDbBridge ?? throw new ArgumentNullException(nameof(petaDbBridge));
        }

        /// <summary>
        /// Fetch all policies
        /// </summary>
        /// <returns></returns>
        public IList<InsurancePolicy> GetAllPolicies()
        {
            return petaDbBridge.GetAllPolicies();
        }

        /// <summary>
        /// Fetch policies by policy number
        /// </summary>
        /// <param name="policyNumber"></param>
        /// <returns></returns>
        public IList<InsurancePolicy> GetPoliciesByNumber(string policyNumber)
        {
            return petaDbBridge.GetPoliciesByNumber(policyNumber);
        }

        /// <summary>
        /// Fetch policies by insured name
        /// </summary>
        /// <param name="lastName"></param>
        /// <param name="firstName"></param>
        /// <returns></returns>
        public IList<InsurancePolicy> GetPoliciesByName(string lastName, string firstName)
        {
            return petaDbBridge.GetPoliciesByName(lastName, firstName);
        }

        /// <summary>
        /// Save policy
        /// </summary>
        /// <param name="policy"></param>
        public void SavePolicy(InsurancePolicy policy)
        {
            if (policy.Insured != null && policy.Insured.MailingAddress != null)
            {
                var insuredAddressId = petaDbBridge.Save(policy.Insured.MailingAddress);
                if (petaDbBridge.IsNew(policy.Insured.MailingAddress))
                {
                    policy.Insured.AddressId = insuredAddressId;
                }
            }

            if (policy.Insured != null)
            {
                var insuredId = petaDbBridge.Save(policy.Insured);
                if (petaDbBridge.IsNew(policy.Insured))
                {
                    policy.InsuredId = insuredId;
                }
            }

            if (policy.RiskAddress != null)
            {
                var riskAddressId = petaDbBridge.Save(policy.RiskAddress);
                if (petaDbBridge.IsNew(policy.RiskAddress))
                {
                    policy.RiskAddressId = riskAddressId;
                }
            }

            var policyId = petaDbBridge.Save(policy);
            if (petaDbBridge.IsNew(policy))
            {
                policy.PolicyId = policyId;
            }
        }

        /// <summary>
        /// Delete policy
        /// </summary>
        /// <param name="policyNumber"></param>
        public void DeletePolicy(string policyNumber)
        {
            var matches = petaDbBridge.GetPoliciesByNumber(policyNumber);
            var found = matches.FirstOrDefault(p => p.PolicyNumber == policyNumber);
            if (found == null)
            {
                throw new Exception($"Cannot delete policy: policy number {policyNumber} not found in data store.");
            }

            petaDbBridge.Delete(found);

            // find matching join records & also delete
            var insuredInfo = petaDbBridge.GetInsured(found.InsuredId);
            if (insuredInfo != null)
            {
                petaDbBridge.Delete(insuredInfo);

                var insuredAddressInfo = petaDbBridge.GetAddress(insuredInfo.AddressId);
                if (insuredAddressInfo != null)
                {
                    petaDbBridge.Delete(insuredAddressInfo);
                }
            }

            var addressInfo = petaDbBridge.GetAddress(found.RiskAddressId);
            if (addressInfo != null)
            {
                petaDbBridge.Delete(addressInfo);
            }
        }
    }
}