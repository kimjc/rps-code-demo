﻿using System;
using System.Collections.Generic;
using RpsWebService.Interfaces;
using RpsWebService.Models;

namespace RpsWebService.Services
{
    /// <summary>
    /// Loose coupling layer to the petapoco database implementation
    /// </summary>
    public class PetaDbBridge : IPetaDbBridge
    {
        private readonly IPetaDbBridgeHelper helper;
        private readonly IPetaDatabase db;

        public PetaDbBridge(IPetaDbBridgeHelper helper, IPetaDatabase db)
        {
            this.db = db ?? throw new ArgumentNullException(nameof(db));
            this.helper = helper ?? throw new ArgumentNullException(nameof(helper));
        }

        /// <summary>
        /// Fetch all policies
        /// </summary>
        /// <returns></returns>
        public IList<InsurancePolicy> GetAllPolicies()
        {
            return helper.GetPoliciesByQuery("select * from policy");
        }

        /// <summary>
        /// Fetch policies by policy number match
        /// </summary>
        /// <param name="policyNumber"></param>
        /// <returns></returns>
        public IList<InsurancePolicy> GetPoliciesByNumber(string policyNumber)
        {
            return helper.GetPoliciesByQuery("select * from policy where policy_number like @0", $"{policyNumber}%");
        }

        /// <summary>
        /// Fetch policies by insured name
        /// </summary>
        /// <param name="lastName"></param>
        /// <param name="firstName"></param>
        /// <returns></returns>
        public IList<InsurancePolicy> GetPoliciesByName(string lastName, string firstName)
        {
            return helper.GetPoliciesByQuery(
                "select p.* from policy p inner join insured i on p.insured_id = i.insured_id where i.last_name like @0 and i.first_name like @1",
                $"{lastName}%", $"{firstName}%");
        }

        /// <summary>
        /// Fetch insured info by insured id
        /// </summary>
        /// <param name="insuredId"></param>
        /// <returns></returns>
        public Insured GetInsured(int insuredId)
        {
            return helper.GetInsured(insuredId);
        }

        /// <summary>
        /// Fetch requested address info by address id
        /// </summary>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public Address GetAddress(int addressId)
        {
            return helper.GetAddress(addressId);
        }

        /// <summary>
        /// Petapoco implementation of a new record check (works fine w/ SQL server, SQLite...)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="payload"></param>
        /// <returns></returns>
        public bool IsNew<T>(T payload)
        {
            return db.IsNew(payload);
        }

        /// <summary>
        /// Save mapped POCO to persistence layer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="payload"></param>
        /// <returns></returns>
        public int Save<T>(T payload)
        {
            return db.Save(payload);
        }

        /// <summary>
        /// Delete mapped POCO from persistence layer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="payload"></param>
        public void Delete<T>(T payload)
        {
            db.Delete(payload);
        }
    }
}