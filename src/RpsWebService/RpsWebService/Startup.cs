﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RpsWebService.Controllers;
using RpsWebService.Interfaces;
using RpsWebService.Models;
using RpsWebService.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace RpsWebService
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        private readonly string rootPath;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            rootPath = env.ContentRootPath;

            PetaDatabase.SetConnectionString(Configuration.GetConnectionString("RpsDemoDatabase"));
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            services.AddMvc();

            // register swagger services
            services.AddSwaggerGen(cfg =>
            {
                cfg.IncludeXmlComments($"{rootPath}\\bin\\Debug\\net452\\win7-x86\\RpsWebService.xml");
                cfg.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = nameof(PolicyInfoController),
                    Description = "REST api for the RPS coding demo",
                });
            });

            // application-specific registrations
            services.AddTransient<IPolicyRepository, PolicyRepository>();
            services.AddTransient<IPetaDbBridge, PetaDbBridge>();
            services.AddTransient<IPetaDbBridgeHelper, PetaDbBridgeHelper>();
            services.AddTransient<IPetaDatabase, PetaDatabase>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseCors("CorsPolicy");

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(cfg =>
            {
                cfg.SwaggerEndpoint("/swagger/v1/swagger.json", $"{nameof(PolicyInfoController)} v1");
            });
        }
    }
}
