export class PolicyItem{
  policyId : string;
  policyNumber : string;
  effectiveDate : Date;
  expirationDate : Date;
  insuredId : Number;
  insured : Insured;
  riskConstruction : string;
  riskYearBuilt : Number;
  riskAddressId : Number;
  riskAddress : Address;
}

export class Insured{
  insuredId : Number;
  firstName : string;
  lastName : string;
  addressId : Number;
  mailingAddress : Address;
}

export class Address{
  addressId : Number;
  mailingAddress : string;
  city : string;
  state : string;
  postal : string;
}

export interface PolicyListItem {
  policyNumber : string;
  insuredName : string;
  effectiveDate : Date;
  expirationDate : Date;
}
