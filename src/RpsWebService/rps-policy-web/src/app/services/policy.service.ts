import {Injectable, Input, Output, EventEmitter} from '@angular/core';
import {Headers, Http, Response, URLSearchParams} from '@angular/http';
import {HttpClient} from '@angular/common/http'

import {Observable} from 'rxjs/Rx';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {ApiService} from './api.service';
import {environment} from './environment';
import {PolicyItem, Insured, Address} from '../models';

@Injectable()
export class PolicyService {

  private subject = new Subject<any>();

  constructor (
    private apiService: ApiService,
    private http : Http,
    private httpc : HttpClient
  ) {}

  sendMessage(message: string) {
      this.subject.next({ text: message });
  }

  sendPolicyMessage(message : any) {
    this.subject.next({item : message})
  }

  clearMessage() {
      this.subject.next();
  }

  getMessage(): Observable<any> {
      return this.subject.asObservable();
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    return new Headers(headersConfig);
  }

  private formatErrors(error: any) {
     return Observable.throw(error.json());
  }

  getAllPolicies(){
    return this.apiService.get('/policy')
      .map(data => data);
  }

  getPoliciesByNumber(policyNum){
    return this.apiService.get(`/policy/${policyNum}`)
      .map(data => data);
  }

  getPoliciesByName(last, first){
    return this.apiService.get(`/policy/${last}/${first}`)
      .map(data => data);
  }

  updatePolicy(policy){
    var result = this.apiService.put('/policy', policy);
    this.subject.next({fetch: "itemupdated"});
    return result;
  }

  updatePolicies(policyArray){
    var result = this.apiService.put('/policy/bulkput', policyArray);
    this.subject.next({fetch: "bulkupdated"});
    return result;
  }

  deletePolicy(policyNum){
    var result = this.apiService.delete(`/policy?policyNumber=${policyNum}`);
    this.subject.next({fetch: "itemdeleted"});
    return result;
  }
}
