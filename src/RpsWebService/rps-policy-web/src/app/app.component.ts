import {Component} from '@angular/core';
import {HeaderComponent} from './header/header.component';

@Component({
  selector: 'material-app',
  templateUrl: 'app.component.html'
})
export class AppComponent {
}
