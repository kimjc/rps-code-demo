import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ConfirmDialogComponent} from './confirm-dialog/confirm-dialog.component';
import {MatDialogRef, MatDialog} from '@angular/material';
import {PolicyItem} from '../models';

@Injectable()
export class DialogsService {

    constructor(private dialog: MatDialog) { }

    public confirm(policy : PolicyItem): Observable<any> {
        let dialogRef: MatDialogRef<ConfirmDialogComponent>;

        dialogRef = this.dialog.open(ConfirmDialogComponent);
        dialogRef.componentInstance.setPolicy(policy);

        return dialogRef.afterClosed();
    }

    public confirmNoDelete(policy : PolicyItem): Observable<any> {
      let dialogRef: MatDialogRef<ConfirmDialogComponent>;

      dialogRef = this.dialog.open(ConfirmDialogComponent);
      dialogRef.componentInstance.setPolicy(policy);
      dialogRef.componentInstance.setNoDelete(true);

      return dialogRef.afterClosed();
    }
}
