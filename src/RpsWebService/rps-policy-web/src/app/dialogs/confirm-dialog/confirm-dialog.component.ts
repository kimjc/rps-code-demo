import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {MatToolbar} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {MatSelect}  from '@angular/material';

import {PolicyItem, Insured, Address} from '../../models';


@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent {

    private policy : PolicyItem;

    constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>) {
    }

    noDelete = false;

    policyNumber : string;
    effectiveDate : Date;
    expirationDate : Date;

    insuredLastName : string;
    insuredFirstName : string;
    insuredMailingAddress : string;
    insuredCity : string;
    insuredState : string;
    insuredPostal : string;

    selectedRiskConstruction : string;
    riskYearBuilt : Number;
    riskAddress : string;
    riskMailingAddress : string;
    riskCity : string;
    riskState : string;
    riskPostal : string;

    riskConstructions = [
      {value: 'SiteBuiltHome', viewValue: 'Site Built Home'},
      {value: 'ModularHome', viewValue: 'Modular Home'},
      {value: 'SingleWideManufacturedHome', viewValue: 'Single Wide Manufactured Home'},
      {value: 'DoubleWideManufacturedHome', viewValue: 'Double Wide Manufactured Home'}
    ];

    public setPolicy(policyItem){
      this.policy = policyItem;
      this.loadPolicyToForm();
    }

    public setNoDelete(noDeleteFlag){
      this.noDelete = noDeleteFlag;
    }

    loadPolicyToForm(){
      if (this.policy == null) return;

      this.policyNumber = this.policy.policyNumber;
      this.effectiveDate = this.policy.effectiveDate;
      this.expirationDate = this.policy.expirationDate;

      if (this.policy.insured != null){
        this.insuredLastName = this.policy.insured.lastName;
        this.insuredFirstName = this.policy.insured.firstName;

        if (this.policy.insured.mailingAddress != null){
          this.insuredMailingAddress = this.policy.insured.mailingAddress.mailingAddress;
          this.insuredCity = this.policy.insured.mailingAddress.city;
          this.insuredState = this.policy.insured.mailingAddress.state;
          this.insuredPostal = this.policy.insured.mailingAddress.postal;
        }
      }

      this.selectedRiskConstruction = this.policy.riskConstruction;
      this.riskYearBuilt = this.policy.riskYearBuilt;

      if (this.policy.riskAddress != null){
        this.riskAddress = this.policy.riskAddress.mailingAddress;
        this.riskCity = this.policy.riskAddress.city;
        this.riskState= this.policy.riskAddress.state;
        this.riskPostal = this.policy.riskAddress.postal;
      }
    }

    loadFormToPolicy(){
      this.policy = this.policy || new PolicyItem();
      this.policy.policyNumber = this.policyNumber;
      this.policy.effectiveDate = this.effectiveDate;
      this.policy.expirationDate = this.expirationDate;

      this.policy.insured = this.policy.insured || new Insured();
      this.policy.insured.lastName = this.insuredLastName;
      this.policy.insured.firstName = this.insuredFirstName;
      this.policy.insured.mailingAddress = this.policy.insured.mailingAddress || new Address();
      this.policy.insured.mailingAddress.mailingAddress = this.insuredMailingAddress;
      this.policy.insured.mailingAddress.city = this.insuredCity;
      this.policy.insured.mailingAddress.state = this.insuredState;
      this.policy.insured.mailingAddress.postal = this.insuredPostal;

      this.policy.riskConstruction = this.selectedRiskConstruction;
      this.policy.riskYearBuilt = this.riskYearBuilt;
      this.policy.riskAddress = this.policy.riskAddress || new Address();
      this.policy.riskAddress.mailingAddress = this.riskAddress;
      this.policy.riskAddress.city = this.riskCity;
      this.policy.riskAddress.state = this.riskState;
      this.policy.riskAddress.postal = this.riskPostal;
    }

    closeDialog(result){
      let localresult = result;

      if (result == 'cancel'){
        this.dialogRef.close(null);
        return;
      }

      if (result == 'update'){
        this.loadFormToPolicy();
      }

      this.dialogRef.close(
        {
          item : this.policy,
          operation : result
        });
    }
}
