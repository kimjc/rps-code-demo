import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';

import {PolicyListComponent} from './policylist.component';
import {PolicyService} from '../services';
import {ApiService} from '../services';
import {DialogsModule} from '../dialogs/dialogs.module';

import {
  MatCardModule,
  MatSortModule,
  MatTableModule,
} from '@angular/material';

@NgModule({
  imports: [
    HttpModule,
    HttpClientModule,
    CommonModule,
    MatCardModule,
    MatSortModule,
    MatTableModule,
    DialogsModule
  ],
  declarations: [
    PolicyListComponent
  ],
  exports:[
    PolicyListComponent
  ],
  providers: [ApiService, PolicyService]
})
export class PolicyListModule {}
