import {Component, ViewChild, OnInit, OnDestroy} from '@angular/core';
import {DatePipe} from '@angular/common';
import {VERSION} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {MatSort} from '@angular/material';

import {Subscription} from 'rxjs/Subscription';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';

import {PolicyListItem} from '../models'
import {PolicyService} from '../services/policy.service';
import {DialogsService} from '../dialogs/dialogs.service';

@Component ({
  selector: 'policy-list',
  templateUrl: './policylist.component.html',
  styleUrls: ['./policylist.component.css'],
  providers: []
})

export class PolicyListComponent implements OnInit, OnDestroy {
  message: any;
  subscription : Subscription;

  constructor(private policyService : PolicyService, private dialogsService : DialogsService){
   this.subscription = this.policyService.getMessage().subscribe(msg => this.onItemUpdated(msg));
  }

  displayedColumns = ['policyNumber', 'insuredName', 'effectiveDate', 'expirationDate'];
  policyDatabase = new PolicyDatabase();

  dataSource : PolicyListDataSource | null;
  @ViewChild(MatSort) sort: MatSort;

  version = VERSION;
  dataSubject = new BehaviorSubject([{id: 1, name: 'Andrew'}]);

  ngOnInit(){
    this.refreshPolicyList();
    this.dataSource = new PolicyListDataSource(this.policyDatabase, this.sort);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onItemUpdated(msg){
    if (msg.item != null){
      let msgPolicy = msg.item;

      let rowItem =
      {
        policyNumber: msgPolicy.policyNumber,
        insuredName: `${msgPolicy.insured.firstName} ${msgPolicy.insured.lastName}`,
        effectiveDate: msgPolicy.effectiveDate,
        expirationDate: msgPolicy.expirationDate
      }
      this.policyDatabase.addPolicy(rowItem);
      return;
    }

    if (msg.text != 'refresh') return;
    this.refreshPolicyList();
  }

  rowItemClick(row){
    this.policyService.getPoliciesByNumber(row.policyNumber)
      .subscribe(found =>{
        if (found.length > 0){
          this.dialogsService
            .confirm(found[0])
            .subscribe(res => {
              if (res == null) return;

              if (res.operation == 'update'){
                this.updatePolicy(res.item);
              }

              if (res.operation == 'delete'){
                this.deletePolicy(res.item);
              }
            });
        }
      });
  }

  addPolicy(policy){
    this.policyDatabase.addPolicy(policy);
    this.policyService.updatePolicy(policy)
      .subscribe(result => {
        console.log(result);
    });
  }

  updatePolicy(policy){
    this.policyDatabase.updatePolicy(policy);
    this.policyService.updatePolicy(policy)
      .subscribe(result => {
        console.log(result);
    });
  }

  deletePolicy(policy){
    this.policyDatabase.removePolicy(policy);
    this.policyService.deletePolicy(policy.policyNumber)
      .subscribe(result =>{
        console.log(result);
      })
  }

  refreshPolicyList(){
    this.policyService.getAllPolicies()
      .subscribe(allPolicies => {

        this.policyDatabase.clearPolicies();
        this.bulkupdate(allPolicies);
      });
  }

  bulkupdate(policies){
    var listOfPolicies : PolicyListItem[] = [];

    for(let i = 0; i < policies.length; i++){
      let p = policies[i];
      listOfPolicies.push({
        policyNumber: p.policyNumber,
        insuredName: `${p.insured.firstName} ${p.insured.lastName}`,
        effectiveDate: p.effectiveDate,
        expirationDate: p.expirationDate
      });
    }

    this.policyDatabase.bulkupdate(listOfPolicies);
  }
}

export class PolicyDatabase {
  dataChange: BehaviorSubject<PolicyListItem[]> = new BehaviorSubject<PolicyListItem[]>([]);
  get data(): PolicyListItem[] { return this.dataChange.value; }

  constructor(){
  }

  clearPolicies(){
    this.dataChange.next([]);
  }

  bulkupdate(policies){
    this.dataChange.next(policies);
  }

  addPolicy(policy){
    const copiedData = this.data.slice();
    copiedData.push(policy);
    this.dataChange.next(copiedData);
  }

  updatePolicy(policy){
    const copiedData = this.data.slice();

    let idx = -1;
    for (let i = 0; i < copiedData.length; i++){
      let rowItem = copiedData[i];
      if (rowItem.policyNumber == policy.policyNumber){
        idx = i;

        rowItem.insuredName = `${policy.insured.firstName} ${policy.insured.lastName}`;
        rowItem.effectiveDate = policy.effectiveDate;
        rowItem.expirationDate = policy.expirationDate;

        break;
      }
    }

    if (idx == -1) return;

    this.dataChange.next(copiedData);
  }

  removePolicy(policy){
    const copiedData = this.data.slice();

    let idx = -1;
    for (let i = 0; i < copiedData.length; i++){
      let rowItem = copiedData[i];
      if (rowItem.policyNumber == policy.policyNumber){
        idx = i;
        break;
      }
    }

    if (idx == -1) return;

    copiedData.splice(idx, 1);
    this.dataChange.next(copiedData);
  }
}

export class PolicyListDataSource extends DataSource<any> {
  constructor(private _database : PolicyDatabase, private _sort : MatSort){
    super();
  }

  connect() : Observable<PolicyListItem[]>{
    const displayDataChanges = [
      this._database.dataChange,
      this._sort.sortChange
    ];

    return Observable.merge(...displayDataChanges).map(()=>{
      return this.getSortedData();
    })
  }

  disconnect(){}

  getSortedData() : PolicyListItem[]{
    const data = this._database.data.slice();
    if (!this._sort.active || this._sort.direction == '') { return data;}

    return data.sort((a, b)=>{
      let propertyA : number | Date | string = '';
      let propertyB : number | Date | string = '';

      switch(this._sort.active){
        case 'policyNumber':
          [propertyA, propertyB] = [a.policyNumber, b.policyNumber];
          break;
        case 'insuredName':
          [propertyA, propertyB] = [a.insuredName, a.insuredName];
          break;
        case 'effectiveDate':
          [propertyA, propertyB] = [a.effectiveDate, a.effectiveDate];
          break;
        case 'expirationDate':
          [propertyA, propertyB] = [a.expirationDate, a.expirationDate];
          break;
      }

      let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      let valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction == 'asc' ? 1 : -1);
    });
  }
}
