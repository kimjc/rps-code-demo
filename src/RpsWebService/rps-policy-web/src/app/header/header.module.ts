import {ModuleWithProviders, NgModule} from '@angular/core';
import {HeaderComponent} from './header.component';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';

import {PolicyService} from '../services';
import {ApiService} from '../services';
import {DialogsModule} from '../dialogs/dialogs.module';

import {
  MatButtonModule,
  MatDialogModule,
  MatToolbarModule
} from '@angular/material';

@NgModule({
  imports: [
    DialogsModule,
    MatDialogModule,
    MatToolbarModule,
    MatButtonModule
  ],
  declarations: [
    HeaderComponent
  ],
  exports:[
    HeaderComponent
  ],
  providers: []
})
export class HeaderModule {}
