import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {DialogsService} from '../dialogs/dialogs.service';
import {PolicyItem} from '../models';
import {PolicyService} from '../services/policy.service';

@Component({
  selector: 'layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  constructor(private dialogsService: DialogsService, private policyService : PolicyService) {}

  policyItem = new PolicyItem();

  showPolicyEditor(): void{
    this.dialogsService
      .confirmNoDelete(this.policyItem)
      .subscribe(res => {
        if (res == null) return;

        if (res.operation == 'update'){
          this.addPolicy(res.item);
        }

        if (res.operation == 'delete'){
          this.deletePolicy(res.item);
        }
      });
  }

  refreshList(): void{
    this.policyService.sendMessage('refresh');
  }

  addPolicy(policy){
    this.policyService.sendPolicyMessage(policy)
    this.policyService.updatePolicy(policy)
      .subscribe(result => {
        console.log(result);
        this.policyItem = new PolicyItem();
    });
  }

  deletePolicy(policy){
    this.policyService.deletePolicy(policy)
      .subscribe(result =>{
        console.log(result);
        this.policyItem = new PolicyItem();
      })
  }
}
